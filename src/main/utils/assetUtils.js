import Env from './envUtils';
import ApiUtils from './restAPIUtils';

import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();

const getassetUrl = asset => {
  let urlFragments = asset.url.split('/');
  urlFragments.shift(); // remove lambda fit-in
  urlFragments.shift(); // remove lambda 1080x80
  const fileLocation = urlFragments.join('/');
  const codeMarket = publicRuntimeConfig.CODE_MARKET;
  const market = codeMarket === 'UK' ? '' : codeMarket.toLowerCase() + '.';
  return `https://${market}assets.kfcapi.com/${fileLocation}`;
}

const get = () => {
  const params = {
    grouping: 'brand',
    mode: 'website',
    modifiedSinceDate: `2018-08-02` // date 1st assets were uploaded
  };
  return ApiUtils.get(
    Env.getEnvParam('apiEndpoints.assets', params)
  ).then(assets => {
    // api can return undefined
    if (!assets) return [];

    let assetsByKey = {};
    assets.forEach(asset => {
      asset.url = getassetUrl(asset);
      assetsByKey[asset.key] = asset;
    });
    return assetsByKey;
  })
    .catch(err => console.error('AssetService.get', err));
}

export default {
  get
};