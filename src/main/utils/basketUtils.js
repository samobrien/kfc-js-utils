import PubSub from 'pubsub-js';
import Env from './envUtils';
import utils from './utils';
import { saveState, loadState, removeState } from './localStorage';
import nextCookie from 'next-cookies';
import cookie from 'js-cookie';

export const ProductTypes = {
  menu: 'Menu',
  single: 'Single'
};

const getCookie = (key, ctx) => {
  if(ctx) {
    try {
      const item = nextCookie(ctx);
      if (!item[key]) return undefined;
      const state = JSON.parse(item[key]);
      return state;
    } catch (err) {
      return undefined;
    }
  } else {
    return cookie.getJSON(key);
  }
}

const setCookie = (key, value) => {
  cookie.set(key, value, { expires: 1 });
}

const ORDER_MINIMUM = Env.getEnvParam('ordering.orderMinimum') || 0;
const ORDER_MAXIMUM =
  Env.getEnvParam('ordering.orderMaximum') || Number.MAX_SAFE_INTEGER;
const ORDER_DELIVERY_REDUCTION_THRESHOLD =
  Env.getEnvParam('ordering.orderDeliveryReductionThreshold') || 0;
const ORDER_DELIVERY_FEE_LOW =
  Env.getEnvParam('ordering.orderDeliveryFeeLow') || 0;
const ORDER_DELIVERY_FEE_HIGH =
  Env.getEnvParam('ordering.orderDeliveryFeeHigh') || 0;

class Basket {
  basket = [];
  type = Env.getEnvParam('modeType.delivery');
  basketKey = Env.getEnvParam('constants.basket');
  deliveryAddressKey = Env.getEnvParam('constants.deliveryAddress');
  restaurantKey = Env.getEnvParam('constants.restaurant');
  modifyBasketKey = Env.getEnvParam('constants.modifyBasket');

  constructor() {
    PubSub.subscribe(this.modifyBasketKey, this.storeBasket);
  }
 
  storeBasket = () => {
    saveState(this.basketKey, this.basket);
  }

  getBasketFromStore = () => {
    // initiate basket from localstore
    const basket = loadState(this.basketKey);
    if (basket) {
      this.basket = basket;
    }
    PubSub.publish(this.modifyBasketKey);
  }

  calculateProductTotal = (product) => {
    let totalPrice = product.price;
    if (product.type === ProductTypes.menu && product.subItemList) {
      product.subItemList.forEach((item) => {
        totalPrice += item.price;
      });
    }
    return totalPrice;
  };

  addUpdateToBasket = async (product) => {
    const price = this.calculateProductTotal(product);
    let basket = this.getBasket();
    const foundItems = basket.filter(item => item.id === product.id);
    let foundItem = null;
    if(foundItems) {
      // if product exists in basket check if sub items are the same to increment quantity and avoid duplication
      foundItems.forEach(item => {
        foundItem = item;
        if (product.subItemList && item.subItemList) {
          product.subItemList.forEach((i, index) => {
            if(i.productId !== item.subItemList[index].productId) {
              // products are not the same
              foundItem = null;
            }
          });
        }
      })
    } 
    
    if(foundItem) {
      foundItem.quantity++;
      foundItem.totalPrice = this.calculateTotalPrice(foundItem);
    } else {
      // create new item in basket
      // this doesnt always work locally on chrome and is a known bug with setting cookies
      basket.push({
        uid: utils.randomUUID(),
        id: product.id,
        prefixId: product.prefixId || 0,
        price,
        totalPrice: price, // times the quantity
        name: product.commercialName,
        quantity: 1,
        type: product.type,
        subItemList: product.type === 'Menu' ? product.subItemList : []
      });
    }

    this.setBasket(basket);
  };

  updateProductQuantity = (uid, quantity) => {
    let basket = this.getBasket();
    const product = basket.find(item => item.uid === uid);
    if (product) {
      product.quantity = quantity;
      product.totalPrice = this.calculateTotalPrice(product);
      this.setBasket(basket);
    }
  }

  calculateTotalPrice = product => {
    // calculate price times quantity
    return utils.toSafeNumber(product.price * product.quantity);
  }

  getSubTotal = () => {
    let subTotal = 0;
    let basket = this.getBasket();
    if (basket && basket.length) {
      basket.forEach(p => subTotal += p.totalPrice);
      subTotal = utils.toSafeNumber(subTotal);
    }
    return subTotal;
  }

  getProductCount = id => {
    let basket = this.getBasket();
    const products = basket.filter(item => item.id === id);
    let count = 0;
    products.forEach(item => {
      count += item.quantity;
    });
    return count !== 0 ? count : null;
  }

  getTotalProductCount = () => {
    let total = 0;
    let basket = this.getBasket();
    basket.forEach(p => total += p.quantity);
    return total;
  }

  getTotalPrice = () => {
    const subTotal = this.getSubTotal();
    return utils.toSafeNumber(subTotal + this.calculateDeliveryFee());
  };

  isValidOrder = () => {
    const subTotal = this.getSubTotal();
    if (subTotal < ORDER_MINIMUM || subTotal > ORDER_MAXIMUM) {
      return false;
    }
    return true;
  };

  calculateDeliveryFee = () => {
    const subTotal = this.getSubTotal();
    if (subTotal < ORDER_DELIVERY_REDUCTION_THRESHOLD) {
      return ORDER_DELIVERY_FEE_HIGH;
    } else {
      return ORDER_DELIVERY_FEE_LOW;
    }
  };

  removeFromBasket = async (uid) => {
    let basket = this.getBasket();
    this.setBasket(basket.filter((item) => item.uid !== uid));
  };

  emptyBasket = () => {
    this.basket = [];
    removeState(this.basketKey);
    cookie.remove(this.restaurantKey);
    cookie.remove(this.deliveryAddressKey);
    PubSub.publish(this.modifyBasketKey);
  };

  setType = (orderType) => {
    this.type = orderType;
  };

  getOrderType = () => {
    return this.type;
  }

  getBasket = () => {
    return this.basket;
  };

  setBasket = (basket) => {
    this.basket = basket;
    PubSub.publish(this.modifyBasketKey);
  }

  setRestaurant = (restaurant) => {
    setCookie(this.restaurantKey, restaurant);
    PubSub.publish(this.modifyBasketKey);
  };

  getRestaurant = (ctx) => {
    return getCookie(this.restaurantKey, ctx) || {};
  };

  setDeliveryAddress = deliveryAddressObject => {
    setCookie(this.deliveryAddressKey, deliveryAddressObject);
    PubSub.publish(this.modifyBasketKey);
  };

  getDeliveryAddress = (ctx) => {
    return getCookie(this.deliveryAddressKey, ctx) || {};
  };
}

const BasketUtils = new Basket();
export { BasketUtils };
export default BasketUtils;
