import Utils from './utils';
import envConfigs from '../configs/env';
import getConfig from 'next/config';
import axios from './axiosUtils';
import cache from 'memory-cache';

let memCache = new cache.Cache();
const { publicRuntimeConfig } = getConfig()

let configs;

(() => {
  const defaultConfigs = envConfigs.default;
  const buildConfigs = envConfigs[publicRuntimeConfig.APP_ENV_CODE];
  let env = {
    countryCode: publicRuntimeConfig.COUNTRY_CODE,
    codeMarket: publicRuntimeConfig.CODE_MARKET
  };

  configs = Object.assign(defaultConfigs.default, buildConfigs, env);
})();


class Settings {
  settings = {};
  constructor() {
    const url = '/util/settings?type=brand';
    const cache = memCache.get(url);
    if(cache) {
      this.settings = cache;
    } else {
      axios.get(url)
        .then(res => {
          if (res && res.data) {
            let settings = res.data;
            memCache.put(url, settings, 30 * 60 * 1000);
            this.settings = Utils.mergeDeep(configs, settings);
          } else {
            this.settings = configs;
          }
        });
    }
  }

  getEnvParam = (param, args) => {
    let str;
    if (Boolean(this.settings)) {
      str = Utils.getParamFromObject(this.settings, param, false);
    }
    if (!str) {
      str = Utils.getParamFromObject(configs, param, false);
    }
    if (!args) return str;

    Object.keys(args)
      .forEach((key) => {
        str = str.replace(new RegExp(`{${key}}`, 'gi'), args[key]);
      });

    return str;
  };
}

const EnvUtils = new Settings();
export { EnvUtils };
export default EnvUtils;
