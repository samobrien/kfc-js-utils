import axios from 'axios';
import EnvUtils from './envUtils';
import getConfig from 'next/config';
import { getUserToken, logout } from './authUtils';
import cache from 'memory-cache';
const { publicRuntimeConfig } = getConfig();

let memCache = new cache.Cache();

export const CacheExpiry = {
  SHORT: 5 * 60 * 1000, // 5 mins
  MEDIUM: 15 * 60 * 1000, // 15 mins
  LONG: 30 * 60 * 1000 // 30 mins
}

export const MyAxios = base => method => url => (headers = {}) => body => async cb =>
  axios({
    method,
    headers: { ...headers },
    url: `/${url}`,
    baseURL: base,
    data: body
  })
    .then(res => cb(res.data))
    .catch(err => {
      console.error(err)
      // cb(null, err)
    });

// Connected to Next
export const NextAPI = MyAxios(publicRuntimeConfig.NEXT_API);
export const getNextApi = NextAPI('get');
export const getAuthToken = getNextApi('api/auth')()();

// Connected to KFC API
export const KFCApi = MyAxios(publicRuntimeConfig.KFC_API);
export const getKFCApi = KFCApi('get');
export const postKFCApi = KFCApi('post');

// Connected to CMS
export const CMSApi = MyAxios(`${publicRuntimeConfig.NEXT_API}/cms`);
// export const CMSApi = MyAxios(publicRuntimeConfig.CMS_API);
export const getCMSApi = CMSApi('get');
export const postCMSApi = CMSApi('post');
const CMSheaders = {
  codemarket: publicRuntimeConfig.CODE_MARKET,
  countrycode: publicRuntimeConfig.COUNTRY_CODE
}

export const getDataFromCMS = url => getCMSApi(`api/data/${url}`);
export const getGlobalsHeaderFooter = getDataFromCMS('header_footer')(CMSheaders)();
export const getErrorFromCMS = getDataFromCMS('error')(CMSheaders)();
export const getRestaurantBase = getDataFromCMS('restaurants_all')(CMSheaders)();

export const getPageFromCMS = page =>
  getCMSApi(`api/page/${page === 'home' ? '' : `${page.split('/').join(':')}`}`)(CMSheaders)();

axios.defaults.timeout = 30000;
axios.defaults.baseURL = publicRuntimeConfig.KFC_API;
axios.defaults.headers.common['x-api-key'] = publicRuntimeConfig.API_KEY;
axios.defaults.headers.common['codemarket'] = publicRuntimeConfig.CODE_MARKET;
axios.defaults.headers.common['countrycode'] = publicRuntimeConfig.COUNTRY_CODE;
axios.defaults.headers.common['langcode'] = publicRuntimeConfig.LANGCODE;

const handleError = (error, callback) => {
  if (!axios.isCancel(error)) {
    if (error && error.response && error.response.data) {
      if (callback) {
        error.response.data.error = true;
        callback(error.response.data);
      }
    } else if (callback) {
      const data = {
        error: true,
        message: '500.error'
      };
      callback(data);
    }
  }
};

const mergeTokenHeadersToBodyFor = (response) => {
  if (response.headers && response.headers.token && response.headers.permanenttoken) {
    response.data.token = response.headers.token;
    response.data.permanentToken = response.headers.permanenttoken;
  }

  return response;
};

const get = (
  url,
  config,
  callback,
  cacheExpiry = null
) => {
  const token = getUserToken();
  if (token) {
    axios.defaults.headers.common[
      EnvUtils.getEnvParam('constants.userAuthToken')
    ] = token;
  }
  if(cacheExpiry) {
    const cacheResponse = memCache.get(url);
    if (cacheResponse) {
      if(callback){
        callback(cacheResponse);
      }
      return cacheResponse;
    }
  }
  return axios.get(url, config)
    .then((response) => {
      if (cacheExpiry && response.status === 200 && response.data && !response.data.error) {
        memCache.put(url, response.data, cacheExpiry);
      }
      if (callback) {
        callback(response && response.data ? response.data : response);
      }
      return response && response.data ? response.data : response;
    })
    .catch((error) => {
      handleError(error, callback);
    });
};

const post = (
  url,
  params,
  config,
  callback
) => {
  const token = getUserToken();
  if (token) {
    axios.defaults.headers.common[
      EnvUtils.getEnvParam('constants.userAuthToken')
    ] = token;
  }
  axios.post(url, params, config)
    .then((response) => {
      if (callback) {
        mergeTokenHeadersToBodyFor(response);

        callback(response && response.data ? response.data : response);
      }
    })
    .catch((error) => {
      handleError(error, callback);
    });
};

const put = (
  url,
  params,
  config,
  callback
) => {
  const token = getUserToken();
  if (token) {
    axios.defaults.headers.common[
      EnvUtils.getEnvParam('constants.userAuthToken')
    ] = token;
  }
  axios.put(url, params, config)
    .then((response) => {
      if (callback) {
        callback(response.data);
      }
    })
    .catch((error) => {
      handleError(error, callback);
    });
};

const deleteRequest = (
  url,
  config,
  callback
) => {
  const token = getUserToken();
  if (token) {
    axios.defaults.headers.common[
      EnvUtils.getEnvParam('constants.userAuthToken')
    ] = token;
  }
  axios.delete(url, config)
    .then((response) => {
      if (callback) {
        callback(response.data);
      }
    })
    .catch((error) => {
      handleError(error, callback);
    });
};

const getErrorMessage = (response) => {
  // this is to handle the various messages returned from our kfc api
  let errorMessage;
  if (response) {
    const { message, messages } = response;
    if (typeof message === 'string') {
      errorMessage = message;
    } else if (typeof messages === 'object') {
      const firstMessage = messages[Object.keys(messages)[0]];
      if (typeof firstMessage === 'string') {
        errorMessage = firstMessage;
      }
    }
  }

  if (!errorMessage) {
    errorMessage = '500.error';
  }

  return errorMessage;
};

const invalidTokenResponseStatus = (response) => {
  return (response.status === 401 && response.data.message === EnvUtils.getEnvParam('constants.invalidToken')) ||
    (response.status === 403 && response.data.message === EnvUtils.getEnvParam('constants.tokenExpired'));
};

const invalidUserToken = (response) => {
  return response.status === 401 && response.data.message === EnvUtils.getEnvParam('constants.invalidUserToken');
};

axios.interceptors.response.use(null, async (err) => {
  if (err.response && invalidUserToken(err.response)) {
    // user should be logged out
    logout();
    return err;
  }
  else if (err.response && err.response.status === 401 || err.response.status === 400){
    return err.response.data ? err.response.data : err;
  }
  else if (err.response && invalidTokenResponseStatus(err.response)) {
    // if response is 401 or 403 we try to get another auth token for the API
    const token = await getAuthToken(data => data.token);

    axios.defaults.headers.common[
      EnvUtils.getEnvParam('constants.authTokenHeader')
    ] = token;

    // Else it doesn't get updated in the new request
    if (err.config.headers.auth) err.config.headers.auth = token;

    return axios(err.config);
  }
});

const RestApiUtils = {
  get, post, put, deleteRequest, getErrorMessage
};

export { RestApiUtils };
export default RestApiUtils;
