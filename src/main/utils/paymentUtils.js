import base64 from 'base-64';
import APIUtils from './restAPIUtils';
import Env from './envUtils';
import Utils from './utils';
import basketUtils from './basketUtils';

const paymentUrl = (paymentResponse) => {
  const { postUrl, reference } = paymentResponse;
  return `${postUrl}?reference=${reference}`;
};

const fleetAvailability = () => {
  const address = basketUtils.getDeliveryAddress();
  const restaurant = basketUtils.getRestaurant();
  const { postalcode, street, city } = restaurant;
  const params = {
    address: address.fullAddress,
    restaurant: {
      postalcode,
      street,
      city,
    },
    date: new Date(),
    packageType: 'small',
  };

  return new Promise((resolve) => {
    APIUtils.post(
      Env.getEnvParam('apiEndpoints.fleetAvailability'),
      params,
      {},
      response => resolve(response)
    );
  });
};

const preauthorise = params => {
  return new Promise((resolve) => {
    APIUtils.post(
      Env.getEnvParam('apiEndpoints.paymentPreauthorise'),
      params,
      {},
      response => resolve(response)
    );
  });
}

const validateOrder = async ({ webordernumber, judopayid, amount, externaluserid }) => {
  return await APIUtils.get(
    Env.getEnvParam('apiEndpoints.validateOrder', {
      webordernumber,
      judopayid,
      amount,
      externaluserid
    }),
    {},
    null
  );
}

export default {
  paymentUrl,
  fleetAvailability,
  preauthorise,
  validateOrder
};
