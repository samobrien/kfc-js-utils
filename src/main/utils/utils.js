import { headerProps, urlToPage } from './../constants/config';

import propPath from 'crocks/Maybe/propPath';
import compose from 'crocks/helpers/compose';
import { getAuthToken, getPageFromCMS, getGlobalsHeaderFooter, getKFCApi, getErrorFromCMS} from './restAPIUtils';

export const timeout = async function (ms) {
  return new Promise(res => setTimeout(res, ms))
}

const isCMSPage = pathname => pathname === '/cmspage';

const getCMSPage = aspath => {
  if (aspath === '/') return 'home'
  else return aspath.split('?')[0].slice(1)
}
/****************************************************************
 * This function takes the props coming from Next and process it
 * either on the client or on the server.
 * Apparently Next does it for us with pathname and query!
 ****************************************************************/
export const getURLFromReq = (props) => {
  if (isCMSPage(props.pathname)) return getCMSPage(props.asPath);
  else if (props.pathname === '/') return 'home'
  else if (props.pathname) return props.pathname.slice(1);
  else return undefined
}

const pagesWithAdditionals = async (props, additionalRequest, composeWithActiveItem) => {
  if (additionalRequest === 'error') {
    const data = await getErrorFromCMS(data => data);
    return { data:  digestThePage(data, composeWithActiveItem)};
  }
  if (additionalRequest === 'order-journey') {
    const mainContent = [];
    const anotherData = composeWithActiveItem(mainContent);
    return { data: anotherData }
  }
  if (additionalRequest === 'restaurantList') {
    const mainContent = [{ id: 'find-a-kfc-map', data: [] }];
    const anotherData = composeWithActiveItem(mainContent);
    return { data: anotherData }
  }
  if (additionalRequest === 'singleRestaurant') {
    const token = await getAuthToken(data => data.token);
    const getSingleRestaurant = getKFCApi(`restaurants/${props.query.id}`)({auth: token})({});
    const singleRestaurant = await getSingleRestaurant(data => data)
    const typeOfRestaurant = propPath(['facilities'], singleRestaurant)
      .option([])
      .filter(item => (item.name === 'Food Court' || item.name === 'Express' || item.name === 'Drive Through'));
    const restaurantData = await getOnlyThePage(getTypeOfRestaurant(typeOfRestaurant), composeWithActiveItem);
    const contentTop = propPath(['mainContent', '0'], restaurantData).option({id: 'skip'});
    const contentBottom = propPath(['mainContent', '1'], restaurantData).option({id: 'skip'});
    const mainContent = [
      { id: 'single-restaurant-head', data: singleRestaurant },
      contentTop,
      { id: 'single-restaurant-body', data: singleRestaurant },
      contentBottom,
    ]
    return { data: composeWithActiveItem(mainContent) }
  }
}

export async function getInitialPropsForPages(props, additionalRequest) {
  const apiEndpoint = getApiEndpoint(props);
  const globals = await getGlobalsHeaderFooter(data => data);
  const composeWithBase = composeProperties(globals);
  const composeWithActiveItem = composeWithBase(apiEndpoint);
  if (!!additionalRequest) {
    return pagesWithAdditionals(props, additionalRequest, composeWithActiveItem);
  }
  const data = await getOnlyThePage(apiEndpoint, composeWithActiveItem);
  
  return { data }
}

const restaurantToEndpoint = {
  'Drive Through': 'restaurant-details-drive-through-page',
  'Food Court': 'restaurant-details-food-court-page',
  'Express': 'restaurant-details-express-page'
}
const getTypeOfRestaurant = (typeArray) => {
  if (typeArray.length === 0) return 'restaurant-details-food-court-page';
  else return restaurantToEndpoint[typeArray[0].name];
}
const getApiPageFromUrl = url => urlToPage[url] ? urlToPage[url] : url;
const getApiEndpoint = compose(getApiPageFromUrl, getURLFromReq);
const digestThePage = (data, composeWithActiveItem) => {
  const status = propPath(['components', '0'], data).map(item => item.component ? 200 : 404).option(500);
  const included = propPath(['components'], data).option([]);
  const metaData = propPath(['meta'], data).option([]);
  const mainContent = status === 200
    ? included.map(getPropsData)
    : [{ id: 'error' }]
  return composeWithActiveItem(mainContent, metaData);
}
const getOnlyThePage = async (apiEndpoint, composeWithActiveItem) => {
  const getThePage = getPageFromCMS(apiEndpoint);
  const data = await getThePage(data => digestThePage(data, composeWithActiveItem))
  return data;
}
const getActive = (header=[], endpoint) => {
  // The known exceptions
  if (endpoint === 'find-a-kfc-map') return '/kfc-near-me';
  // The tree search
  const id = `/${endpoint}`;
  const item = header
    .map(item => item.children ? item.children.concat(item) : [item])
    .map(group => group.filter(item => item.href === id))
    .reduce((acc, group, iter) => {
      if (group.length > 0) return header[iter].href;
      else return acc;
    }, '');
  return item;
}

const convertToTextLink = item => ({...item, type: 'text', id: item.href})
const convertToIconLink = item => ({...item, type: 'icon', id: item.url, icon: item.label.toLowerCase(), iconSize: 40});

const getSubMenuDetail = (header=[], endpoint) => {
  const id = getActive(header, endpoint);
  const item = header.filter(item => item.href === id);
  const subMenu = propPath(['0', 'children'], item)
    .map(group => group ? group.map(convertToTextLink) : [])
    .option([]);
  return subMenu;
};

export const getQueryFromUrl = ({ asPath }) => {
  let queryObj = {};
  const queryString = asPath.split('?')[1];
  if(queryString) {
    queryString.split('&').forEach(part => {
      var item = part.split('=');
      queryObj[item[0]] = decodeURIComponent(item[1]);
    });
  }
  return queryObj;
}

const getSubMenuActive = endpoint => `/${endpoint}`;

/*
  digestHeader is responsible to parse the response from the CMS and produce
  a digestable header. I catch here header related errors.
  Trivial errors in headers don't throw errors
*/
const digestHeader = propBase => {
  const menu = propPath(['header'], propBase).map(item => item.map(convertToTextLink)).option([]);
  const utils = headerProps.utils;
  const logo = propPath(['header_logo', 'url'], propBase).option('noLogo');
  return {menu, utils, logo}
}

const chunk = (array, size) => {
  if (!array || !array.length) return [];
  const head = array.slice(0, size);
  const tail = array.slice(size);
  return [head, ...chunk(tail, size)];
}

/*
  Digest Footer is responsible of taking the CMS response from the header
  and digesting in a safe way all the footer related information.
  Trivial errors in Footer don't throw
 */
const digestFooter = (footer) => {
  const [menuC1, menuC2, menuC3] = chunk(
    propPath(['footer_top'], footer)
      .map(item => item.map(convertToTextLink))
      .option([])
    , 4
  );
  const legalMenu = propPath(['footer_bottom'], footer)
    .map(item => item.map(convertToTextLink))
    .option([]);
  const socialMenu = propPath(['social'], footer)
    .map(item => item.map(convertToIconLink))
    .option([]);
  const logo = propPath(['footer_logo', 'url'], footer).option('noLogo')
  return {menuC1, menuC2, menuC3, legalMenu, socialMenu, logo}
}

const digestAppBanner = (banner = {}) => ({
  visible: true,
  backgroundColor: 'grey',
  data: banner.app_banner
});

const composeProperties = propBase => endpoint => (content, meta) => {
  const header = propBase ? propBase.header : [];
  return ({
    meta: meta,
    header: digestHeader(propBase),
    appBanner: digestAppBanner(propBase),
    footer: digestFooter(propBase),
    mainContent: content,
    activeHeaderItem: getActive(header, endpoint),
    hasSubMenu: getSubMenuDetail(header, endpoint).length > 0,
    subMenuDetail: getSubMenuDetail(header, endpoint),
    subMenuActive: getSubMenuDetail(header, endpoint).length > 0 ? getSubMenuActive(endpoint) : ''
  }
)};

const getPropsData = (item) => {
  const propsData = {};
  const newComponent = propPath(['component'], item).option(item.type);
  propsData.spaced = propPath(['component_margin'], item).option(false);
  propsData.hideMobile = propPath(['attributes', 'field_hide_copy_on_mobile'], item).option(false);
  switch (newComponent) {
    case 'carousel':
    case 'large_carousel':
      propsData.indented = propPath(['indented_sides'], item).option(false);
      const { childrens, carousel } = getCarousel(item.data);
      const realCarousel = carousel.filter(item => (item.src !== 'noImage' || item.video !== 'noVideo'));
      const indexes = realCarousel.map(item => item.position)
      propsData.children = childrens.filter(item => indexes.indexOf(item.position) > -1);
      propsData.carousel = realCarousel;
      break;
    case 'image_banner':
      const carouselProps = [{
        justifyContent: 'center',
        type: 'image',
        src: propPath(['data', 'image', 'original', 'url'], item).option('noImage'),
        meta: propPath(['data', 'image', 'original'], item).option({}),
        altSrc: propPath(['data', 'image_alt', 'original', 'url'], item).option(''),
        altMeta: propPath(['data', 'image_alt', 'original'], item).option({}),
        position: 1,
        opacity: 0,
        key: `img_banner${Math.random()}`
      }]
      propsData.carousel = carouselProps;
      break;
    case 'two_column_cta':
      const def = ['left', 'right'];
      const content = def.map(side => item.data[side])
      propsData.children = content;
      break;
    case 'find_a_kfc_bar':
      propsData.children = propPath(['data', 'variant'], item).option('search');
      break;
    case 'concertina':
      propsData.heading = propPath(['attributes', 'field_heading'], item).option('');
      propsData.children = propPath(['data'], item).option({});
      break;
    default: 
      propsData.children = propPath(['data'], item).option({});
      break;
  }
  return { id: newComponent, data: propsData }
}

const getCarousel = (data) => ({
  childrens: getCarouselChildrens(data),
  carousel: getCarouselContent(data)
})

const human2flex = item => {
  switch (item) {
    case 'center':
      return 'center'
    case 'left':
      return 'flex-start'
    case 'right':
      return 'flex-end'
    default:
      return 'center'
  }
}
const getCarouselChildrens = data => Object.keys(data).map(key => {
  const slide = data[key];
  return {
    type: propPath(['style'], slide).option('empty-block'),
    alignItems: 'center',
    justifyContent: 'center',
    blockBehindText: propPath(['content_background'], slide).option(''),
    title: propPath(['heading'], slide).option('Title'),
    titleColor: propPath(['text_colour'], slide).option('white'),
    paragraph: slide.body,
    paragraphColor: propPath(['text_colour'], slide).option('white'),
    transparent: propPath(['content_background'], slide).map(item => item === false ? 'transparent' : null),
    buttons: ['', '2'].map(infix => ({
      ctaButton: propPath([`button${infix}_text`], slide).option(''),
      ctaButtonStyle: propPath([`button${infix}_style`], slide)
        .map(style => style.toLowerCase())
        .map(item => item === 'quaternary' ? 'white' : item)
        .option('primary'),
      ctaButtonLink: propPath([`button${infix}_link`, 'uri'], slide).option('#'),
      ctaButtonLinkTarget: propPath([`button${infix}_link_target`], slide).map(target => target === 'new_tab' ? '_blank' : '_self').option('')
    })),
    position: propPath(['position'], slide).option('10')
  }
}).sort((a, b) => b.position < a.position)

const getCarouselContent = data => Object.keys(data).map(key => {
  const slide = data[key];
  return {
    justifyContent: propPath(['content_alignment'], slide).map(item => human2flex(item)).option('center'),
    src: propPath(['image', 'original', 'url'], slide).option('noImage'),
    mobileSrc: propPath(['image_alt', 'original', 'url'], slide).option(''),
    videoSrc: propPath(['video_uri'], slide).option('noVideo'),
    type: propPath(['video_uri'], slide).option('noVideo') !== 'noVideo' ? 'video' : 'image',
    meta: propPath(['image', 'original'], slide).option({}),
    position: propPath(['position'], slide).option('10'),
    opacity: propPath(['image_opacity_overlay'], slide).map(value => value === true ? 0.3 : 0).option(0),
    key
  }
}).sort((a, b) => b.position < a.position)

const getJSONBody = data => getJSONBodyProp(data)
  .map(x => JSON.parse(x))
  .option({});
const getJSONBodyProp = propPath(['resolvedResource#uri{0}', 'body']);

/**
* debounces a function based on a timeout
* @param {function} fn - Function to be executed 
* @param {number} delay - Time of the debounce
*/
const debounce = (fn, delay) => {
  let timer = null;
  return function (...args) {
    const context = this;
    timer && clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(context, args);
    }, delay);
  };
};

const getDayFromDate = (date) => {
  const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
  const today = date || new Date();
  return days[today.getDay()];
};

const sortByProp = (key) => {
  return (a, b) => {
    if (a[key] < b[key])
      return -1;
    if (a[key] > b[key])
      return 1;
    return 0;
  }
};

const getGeolocation = (success, error) => {
  if (navigator.geolocation) {
    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    return navigator.geolocation.getCurrentPosition(success, error, options);
  } else {
    return error;
  }
};

const getParamFromObject = (obj, path, def) => {
  const fullPath = path
    .replace(/\[/g, '.')
    .replace(/]/g, '')
    .split('.')
    .filter(Boolean);
  const everyFunc = (step) => {
    return !(step && (obj = obj[step]) === undefined);
  };
  return fullPath.every(everyFunc) ? obj : def;
};

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

const isObject = (item) => {
  return (item && typeof item === 'object' && !Array.isArray(item));
};

const mergeDeep = (target, ...sources) => {
  if (!sources.length) return target;
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach((key) => {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        mergeDeep(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    });
  }

  return mergeDeep(target, ...sources);
};

function randomUUID() {
  return `${s4() + s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
}

function toSafeNumber(number) {
  // to deal problems with floating-point logic and arithmetic
  return Number(number.toFixed(2));
}

function getRoute(routes, name) {
  if (routes && routes.routes) {
    const route = routes.routes.find(route => route.name === name);
    if (route && route.pattern) return route.pattern;
  }
  return '/';
}

const convertToBoolean = obj => {
  // horrible recursive code to fix broken API responses
  for(var key in obj) {
      const value = obj[key];
      obj[key] = typeof value === 'object' ? 
        convertToBoolean(value) : 
        value != undefined && (value === 'true' || value === true) ? true : (value === 'false' || value === false) ? false : value;
  }
  return obj;
}

export default {
  convertToBoolean,
  getRoute,
  getDayFromDate,
  debounce,
  timeout,
  sortByProp,
  getGeolocation,
  randomUUID,
  mergeDeep,
  getParamFromObject,
  toSafeNumber
};
