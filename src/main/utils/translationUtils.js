
import Env from './envUtils';
import ApiUtils, { CacheExpiry } from './restAPIUtils';
import Utils from './utils';
import translations from '../configs/localization';
import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig()

let configs = {}; 

(() => {
  configs = translations[publicRuntimeConfig.APP_ENV_CODE]
})();


class Translations {
  translations = null;
  constructor(translations){
    this.translations = Utils.mergeDeep(configs, translations);
  }

  get = (param, args) => {
    let str;
    if (this.translations) {
      str = Utils.getParamFromObject(this.translations, param, param);
    }
    if (!str) {
      str = Utils.getParamFromObject(configs, param, param);
    }
    if (!args) return str;

    Object.keys(args)
      .forEach((key) => {
        str = str.replace(new RegExp(`{${key}}`, 'gi'), args[key]);
      });

    return str;
  };
}

const get = async () => {
  const translations = await ApiUtils.get(
    Env.getEnvParam('apiEndpoints.translations'),
    {},
    null,
    CacheExpiry.LONG
  )
  return new Translations(translations);
}

export default {
  get
};