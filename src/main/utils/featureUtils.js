import featuresConfig from '../configs/features';
import ApiUtils, { CacheExpiry } from './restAPIUtils';
import Utils from './utils';
import getConfig from 'next/config';
import Env from './envUtils';

const { publicRuntimeConfig } = getConfig()

let configs = {}; 

(() => {
  const defaultConfigs = featuresConfig.default;
  const buildConfigs = featuresConfig[publicRuntimeConfig.APP_ENV_CODE];
  configs = Object.assign(defaultConfigs, buildConfigs);
})();

const isAvailable = (features, feature) => {
  return Utils.getParamFromObject(features, feature, undefined);
};

const get = async () => {
  const featuresApi = await ApiUtils.get(
    Env.getEnvParam('apiEndpoints.features'),
    {},
    null,
    CacheExpiry.LONG
  );
  const convertedFeatures = Utils.convertToBoolean(featuresApi);
  return Utils.mergeDeep(configs, convertedFeatures);
}

export default {
  get,
  isAvailable
};