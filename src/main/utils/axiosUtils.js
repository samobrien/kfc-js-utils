import axios from 'axios';
import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();

axios.defaults.timeout = 30000;
axios.defaults.baseURL = publicRuntimeConfig.KFC_API;
axios.defaults.headers.common['x-api-key'] = publicRuntimeConfig.API_KEY;
axios.defaults.headers.common['codemarket'] = publicRuntimeConfig.CODE_MARKET;
axios.defaults.headers.common['countrycode'] = publicRuntimeConfig.COUNTRY_CODE;
axios.defaults.headers.common['langcode'] = publicRuntimeConfig.LANGCODE;

export default axios;