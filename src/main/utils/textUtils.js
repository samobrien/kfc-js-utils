import Env from './envUtils';

const MOBILE_REGEX = /\s*((\s?\d)([-\s]?\d){9}|[(](\s?\d)([-\s]?\d)+\s*[)]([-\s]?\d)+)\s*/;
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_BASIC_REGEX = /^(?!.* )(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}$/; // Uppercase characters, Lowercase characters, Numeric characters, no spaces and min 8 characters
const NUMBER_REGEX = /^-?\d*(\.\d+)?$/;
const CURRENCY_REGEX = /^\d+(\.|\,)\d\d$/;
const LOYALTY_CARD_REGEX = /^\d{8}-?\d{3}$/;

const isValidEmail = (email) => {
  return EMAIL_REGEX.test(email);
};

const isValidPassword = (password) => {
  if (!password || !PASSWORD_BASIC_REGEX.test(password)) {
    return false;
  }

  return true;
};

const isValidCard = (number) => {
  return LOYALTY_CARD_REGEX.test(number);
};

const isValidName = (text) => {
  return text && text.length >= 2;
};

const isValidMobileLength = (phone) => {
  return phone && phone.length >= 10 && phone.length < 12;
}

const isValidConfirmPassword = (password, confirmPassword) => {
  return password === confirmPassword;
};

const isValidMobile = (text) => {
  return MOBILE_REGEX.test(text);
};

const isValidNumber = (text) => {
  return NUMBER_REGEX.test(text);
};

const isValidCurrency = (text) => {
  return CURRENCY_REGEX.test(text);
};

const displayPrice = (price) => {
  let priceFloat = parseFloat(price);
  const symbol = Env.getEnvParam('currency.symbol');
  const smallSymbol = Env.getEnvParam('currency.smallSymbol');
  const separator = Env.getEnvParam('currency.separator');
  const position = Env.getEnvParam('currency.position');
  const space = Env.getEnvParam('currency.space') ? ' ' : '';

  if (!priceFloat) {
    priceFloat = 0;
  }

  if (smallSymbol && priceFloat < 1 && priceFloat > 0 && priceFloat != 0) {
    priceFloat = Math.floor(priceFloat * 100);
    return `${priceFloat}${smallSymbol}`;
  } else {
    const priceInString = priceFloat.toFixed(2).replace(/\./g, separator);
    if (position === 'start') {
      return `${symbol}${space}${priceInString}`;
    } else {
      return `${priceInString}${space}${symbol}`;
    }
  }
};

const defaultOptions = {
  inputFormat: 'sentence',
  outputFormat: 'normal'
};

const titleCase = (str, customOptions) => {
  const options = Object.assign({}, defaultOptions, customOptions);
  if (options.inputFormat === 'pascal' || options.inputFormat === 'camel') {
    str = str.replace(/([A-Z])/g, ' $1');
  }
  str = str.toLowerCase();
  str = str.split(' ');
  for (let i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  str = str.join(' ').trim();

  if (options.outputFormat === 'upper') {
    str = str.toUpperCase();
  }

  return str;
};

// Convert number (must be supplied in meters) to display format
const displayDistance = (value) => {
  if (Env.getEnvParam('distanceFormat') === 'mi') {
    return `${(value / 1609.344).toFixed(1)} mi`;
  }
  return `${(value / 1000.0).toFixed(1)} km`;
};

const toUpperCase = (str) => {
  if (str === null || str === undefined) return false;

  return str.toUpperCase();
};

const toLowerCase = (str) => {
  if (str === null || str === undefined) return false;

  return str.toLowerCase();
};

const truncate = (str, limit) => {
  return !str || str.length < limit ? str : str.substr(0, limit).trim() + '...';
};

const stringWithParam = (str, args) => {
  if (!str) return '';
  Object.keys(args)
    .forEach((key) => {
      str = str.replace(new RegExp(`{${key}}`, 'gi'), args[key]);
    });
  return str;
}

export default {
  isValidEmail,
  isValidPassword,
  isValidName,
  isValidMobile,
  isValidConfirmPassword,
  isValidCard,
  displayPrice,
  titleCase,
  isValidNumber,
  isValidCurrency,
  displayDistance,
  toUpperCase,
  toLowerCase,
  truncate,
  stringWithParam,
  isValidMobileLength
};
