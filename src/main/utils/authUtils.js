import { Component } from 'react';
import Router from 'next/router';
import nextCookie from 'next-cookies';
import cookie from 'js-cookie';
import Env from './envUtils';

export const login = async ({ user, token, permanentToken }) => {
  cookie.set(Env.getEnvParam('constants.token'), token, { expires: 1 });
  cookie.set(Env.getEnvParam('constants.user'), user, { expires: 1 });
  cookie.set(Env.getEnvParam('constants.permanentToken'), permanentToken, { expires: 1 });
  cookie.remove(Env.getEnvParam('constants.tempUser'));
}

// used for register verifcation step
export const tempUser = async ({ user }) => {
  cookie.set(Env.getEnvParam('constants.tempUser'), user, { expires: 1 });
}

export const setUserDetails = user => {
  cookie.set(Env.getEnvParam('constants.user'), user, { expires: 1 });
}

export const logout = (redirecUrl) => {
  cookie.remove(Env.getEnvParam('constants.token'));
  cookie.remove(Env.getEnvParam('constants.user'));
  cookie.remove(Env.getEnvParam('constants.permanentToken'));
  cookie.remove(Env.getEnvParam('constants.tempUser'));
  // to support logging out from all windows
  window.localStorage.setItem('logout', Date.now())
  Router.push(redirecUrl || '/');
}

export const getUser = () => cookie.getJSON(Env.getEnvParam('constants.user')) || null;
export const getTempUser = () => cookie.getJSON(Env.getEnvParam('constants.tempUser')) || null;
export const getUserToken = () => cookie.get(Env.getEnvParam('constants.token'));

// Gets the display name of a JSX component for dev tools
const getDisplayName = Component =>
  Component.displayName || Component.name || 'Component'

export const withAuthSync = WrappedComponent =>
  class extends Component {
    static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`

    static async getInitialProps(ctx) {
      const token = auth(ctx)

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx))

      return { ...componentProps, token }
    }

    constructor(props) {
      super(props)

      this.syncLogout = this.syncLogout.bind(this)
    }

    componentDidMount() {
      window.addEventListener('storage', this.syncLogout)
    }

    componentWillUnmount() {
      window.removeEventListener('storage', this.syncLogout)
      window.localStorage.removeItem('logout')
    }

    syncLogout(event) {
      if (event.key === 'logout') {
        console.log('logged out from storage!')
        Router.push('/')
      }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

export const auth = ctx => {
  const { token } = nextCookie(ctx)

  /*
   * This happens on server only, ctx.req is available means it's being
   * rendered on server. If we are on server and token is not available,
   * means user is not logged in.
   */
  if (ctx.req && !token) {
    ctx.res.writeHead(302, { Location: '/' })
    ctx.res.end()
    return
  }

  // We already checked for server. This should only happen on client.
  if (!token) {
    Router.push('/')
  }

  return token
}
