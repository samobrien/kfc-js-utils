const Maybe = require('crocks/Maybe');

export const getDistanceFrom = position => item => Object.assign(
  item,
  {distance: Math.sqrt(
    Math.pow(position.longitude - item.longitude, 2) +
    Math.pow(position.latitude - item.latitude, 2)
  )}
)

export const nearest = (a, b) => (a.distance - b.distance)

export const getRestaurants = ld => restaurants => restaurants
  .map(ld)
  .sort(nearest);

export const defineBoundaries = bounds => item => (
  (item.latitude < bounds.top)
  && (item.latitude > bounds.bottom)
  && (item.longitude > bounds.left)
  && (item.longitude < bounds.right))
  
export const defineMesh = bounds => (xmax, ymax, data) => {
  const grid = [];
  const dx = (bounds.right - bounds.left)/xmax;
  const dy = (bounds.top - bounds.bottom)/ymax;
  let dataToReduce = data.sort((a, b) => (a.longitude - b.longitude));
  for (let i = 0; i < xmax; i++) {
    let dataToWorkWith = [];
    grid[i] = [];
    const xPos = bounds.left + (i+0.5) * dx;
    const xMax = xPos + dx/2;
    let z;
    for (z = 0; z < dataToReduce.length; z++) {
      if (dataToReduce[z].longitude < xMax) {
        dataToWorkWith.push(dataToReduce[z])
      } else break;
    }
    dataToReduce = dataToReduce.slice(z);
    const orderedDataToWorkWith = dataToWorkWith.sort((a, b) => (a.latitude - b.latitude))
    for (let j = 0; j < ymax; j++) {
      const yPos = bounds.bottom + (j+0.5) * dy;
      const yMax = yPos + dy/2;
      let t;
      let squareData = [];
      for (t = 0; t < orderedDataToWorkWith.length; t++) {
        if (orderedDataToWorkWith[t].latitude < yMax) {
          squareData.push(orderedDataToWorkWith[t])
        } else break;
      }
      grid[i][j] = {
        x: bounds.left + (i+0.5) * dx,
        y: bounds.bottom + (j+0.5) * dy,
        num: squareData.length,
        data: squareData
      }
      orderedDataToWorkWith = orderedDataToWorkWith.slice(t);
    }
  }
  return {dx, dy, grid};
}  