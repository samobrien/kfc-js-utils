import React from 'react';
import {RestApiUtils, EnvUtils} from './main';

class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      settings: {}
    }
  }

  componentDidMount(){
    RestApiUtils.get(
      EnvUtils.getEnvParam('apiEndpoints.settings')
    )
    .then(settings => this.setState({ settings }))
  }

  render () {
    const countryCode = EnvUtils.getEnvParam('countryCode');
    const codeMarket = EnvUtils.getEnvParam('codeMarket');
    return (
      <div className="App">
        <h1>App to test Utils integration</h1>
        {/* <p>Translation test: {localizeString('home.registerNow')}</p> */}
        <p>countryCode: {countryCode}</p>
        <p>codeMarket: {codeMarket}</p>
        <p>Settings:</p>
        <textarea style={{height: '300px', width: '100%', fontSize: '17px'}} 
          value={JSON.stringify(this.state.settings)}>
          </textarea>
      </div>
    );
  }
}

export default App;
